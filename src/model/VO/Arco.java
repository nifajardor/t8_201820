package model.VO;

public class Arco {

	private Vertice<String> origen;
	private Vertice<String> destino;
	private double costo;
	public Arco(Vertice<String> origin, Vertice<String> end, double costoo) {
		origen = origin;
		destino = end;
		costo = costoo;
	}
	public Vertice<String> darOrigen() {
		return origen;
	}
	public Vertice<String> darDestino() {
		return destino;
	}
	public double darCosto() {
		return costo;
	}
}
