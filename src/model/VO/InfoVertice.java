package model.VO;

import model.dataStructures.ArregloFlexible;

public class InfoVertice {
	protected double lat;
	protected double lon;
	protected ArregloFlexible<Vertice> adjacencias;
	
	public void adicionarCalle(Vertice adicionar) {
		adjacencias.agregarElem(adicionar);
	}
	public ArregloFlexible<Vertice> darAdjacencias(){
		return adjacencias;
	}
	public double darLat(){
		return lat;
	}
	public double darLon(){
		return lon;
	}
}
