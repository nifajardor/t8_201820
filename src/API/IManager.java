package API;



import java.time.LocalDateTime;

public interface IManager {
    
	/**
	 * Actualizar la informacion del sistema con los datos seleccionados por el usuario y generar/actualizar las estructuras de datos necesarias.
	 * Caso Especial: si rutaTrips y rutaStations son la cadena vacia (""), los datos del sistema deben reiniciarse con un conjunto de trips y de estaciones vacios.
	 * @param rutaTrips ruta del archivo de trips que se va a utilizar
	 * @param rutaStations ruta del archivo de stations que se va a utilizar
	 */
	void cargarDatos(String rutaNodos,String rutaAdyacentes, String rutaStations);
	void anadirVerticesInterseccion();
	void anadirArcosAIntersecciones();
	void anadirEstacionesConSusArcos();
	void exportarJson();
	void importarJson();
	void hacerMapa();
   }