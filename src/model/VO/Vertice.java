package model.VO;

import model.dataStructures.ArregloFlexible;

public class Vertice<K> {
	private K identificacion;
	private InfoVertice infoVertice;
	private ArregloFlexible<Arco> arcos;
	public Vertice(InfoVertice info, K id) {
		infoVertice=info;
		identificacion=id;
		arcos=new ArregloFlexible<Arco>(1);
	}
	public K darId() {
		return identificacion;
	}
	public InfoVertice darInfovertice() {
		return infoVertice;
	}
	public ArregloFlexible<Arco> darArcos(){
		return arcos;
	}
	public void anadirArco(Arco a) {
		arcos.agregarElem(a);
	}
	
}
