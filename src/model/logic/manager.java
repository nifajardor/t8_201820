package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import model.VO.*;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import API.IManager;
import model.VO.Interseccion;
import model.VO.Station;
import model.dataStructures.*;
import model.dataStructures.Grafo;
import model.dataStructures.SeparateChainingHash;

public class manager implements IManager{
	public static final String RUTA_README = "data/README.txt";
	public static final String RUTA_ESTACIONES = "data/Divvy_Stations_2017_Q3Q4.csv";
	public static final String RUTA_NODOS = "data/Nodes_of_Chicago_Street_Lines.csv";
	public static final String RUTA_CALLES = "data/Adjacency_list_of_Chicago_Street_Lines.csv";
	public static final String DATE_PATTERN = "MM/dd/yyyy HH:mm:ss";
	public static final int RADIO_TIERRA = 6371;
	private ArregloFlexible<Station> arregloStats;
	private ArregloFlexible<Interseccion> arregloNodos;
	private LinearProbingHash<String,Vertice<String>> hashNodos;
	private ArregloFlexible<Arco> arregloCalles;
	private Grafo<String, Vertice<String>, Arco> grafo;
	private int verticesEstacion;
	private int arcosMixtos;
	private int verticesInterseccion;
	private int arcosIniciales;
	private ArregloFlexible<Vertice<String>> vertices;

	@Override
	public void cargarDatos(String rutaNodos,String rutaAdyacentes, String rutaStations) {
		verticesEstacion=0;
		arcosMixtos=0;
		verticesInterseccion=0;
		arcosIniciales=0;
		try {


			String[] nodos;
			String[] stat;
			String[] ady;

			vertices = new ArregloFlexible<Vertice<String>>(330000);
			arregloStats = new ArregloFlexible<Station>(626);
			arregloNodos = new ArregloFlexible<Interseccion>(330000);
			arregloCalles = new ArregloFlexible<Arco>(330000);
			hashNodos = new LinearProbingHash<String,Vertice<String>>(330000);

			DateTimeFormatter formato = DateTimeFormatter.ofPattern(DATE_PATTERN);
			int cont = 0;

			CSVReader lectorStations = new CSVReader(new FileReader(rutaStations));
			stat = lectorStations.readNext();
			while((stat = lectorStations.readNext()) != null) {
				int id = Integer.parseInt(stat[0]);
				String statName = stat[1];
				int cp=Integer.parseInt(stat[5]);
				String[]fecha=stat[6].split("(?=\\s)");
				double lat = Double.parseDouble(stat[3]);
				double lon = Double.parseDouble(stat[4]);
				LocalDate startDate = convertirFecha_Hora_LDT(fecha[0], fecha[1]);
				Station estacion = new Station(id, lat, lon);
				String info = estacion.toString();
				System.out.println("Se carg� la estaci�n: "+info);

				//statQueue.enqueue(nodo);
				arregloStats.agregarElem(estacion);
				verticesEstacion++;
				cont++;
			}
			lectorStations.close();


			CSVReader lectorNodos = new CSVReader(new FileReader(rutaNodos));

			int contador = 0;
			nodos = lectorNodos.readNext();
			while((nodos = lectorNodos.readNext()) != null) {

				int id = Integer.parseInt(nodos[0]);
				double lat = Double.parseDouble(nodos[1]);
				double lon = Double.parseDouble(nodos[2]);
				Interseccion inter=new Interseccion(id, lat ,lon);
				arregloNodos.agregarElem(inter);
				hashNodos.put("I-"+id, new Vertice<String>(inter,"I-"+id));
				vertices.agregarElem(new Vertice<String>(inter,"I-"+id));

				verticesInterseccion++;
				System.out.println("I-"+id+":"+lat+"-"+lon);
				//System.out.println(contador+"");
				//	contador++;

			}
			//System.out.println(hashNodos.get("I-300").darId());
			CSVReader lectorCalles = new CSVReader(new FileReader(rutaAdyacentes));


			BufferedReader  reader=new BufferedReader(new FileReader(rutaAdyacentes));
			ady=lectorCalles.readNext();
			//	ady = reader.readLine().split(" ");
			while((ady = lectorCalles.readNext()) != null) {
				//System.out.println(ady[0].split(" ")[0]);
				//int id = Integer.parseInt(ady[0]);

				String[] cadena = ady[0].split(" ");
				Vertice<String> inicial = hashNodos.get("I-"+cadena[0]);

				double latInicial = inicial.darInfovertice().darLat();
				double lonInicial = inicial.darInfovertice().darLon();
				for(int i=1;i<cadena.length;i++){
					Vertice<String> otro = hashNodos.get("I-"+cadena[i]);
					double latFin = otro.darInfovertice().darLat();
					double lonFin = otro.darInfovertice().darLon();
					double distancia = distancia(latInicial, lonInicial, latFin, lonFin);
					Arco anadir = new Arco(inicial, otro,distancia);
					arregloCalles.agregarElem(anadir);
					arcosIniciales++;
					inicial.darId();
					otro.darId();
					System.out.println("V1:"+inicial.darId()+"V2:"+otro.darId()+"D:"+distancia);
					//					System.err.println(latInicial+"-"+lonInicial+"->"+latFin+"-"+lonFin);
					//					System.err.println(distancia(latInicial, lonInicial, latFin, lonFin)+"");
				}


			}
			System.out.println("Total estaciones cargadas en el sistema: "+ cont);


			reader.close();
			lectorNodos.close();
			lectorCalles.close();

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		grafo = new Grafo<String,Vertice<String>,Arco>(verticesEstacion+verticesInterseccion);

	}
	public Vertice<String> darVerticePorId(String id){
		Vertice<String> vertex = null;
		for(int i=0;i<vertices.darTamAct();i++) {
			if(id.equals(((Vertice<String>) vertices.darElementoI(i)).darId())) {
				vertex = (Vertice<String>) vertices.darElementoI(i);
			}
		}
		return vertex;
	}
	@Override
	public void anadirVerticesInterseccion() {
		for(int i =0;i<arregloNodos.darTamAct();i++) {
			Interseccion inter = (Interseccion) arregloNodos.darElementoI(i);
			Vertice<String> vertex = new Vertice<String>(inter, "I-"+inter.darId());
			grafo.adicionarVertice("I-"+inter.darId(), vertex);
			System.out.println("El vertice: "+ vertex.darId()+" ha sido a�adido al grafo");
		}
	}

	public static double distancia(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return RADIO_TIERRA*1000 * c; // <-- d
	}

	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

	private static LocalDate convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0].trim());
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;

		if(datosHora.length>2){
			segundos=Integer.parseInt(datosHora[2]);
		}

		return LocalDate.of(agno, mes, dia);
	}
	@Override
	public void anadirArcosAIntersecciones() {
		// TODO Auto-generated method stub
		for(int i=0;i<arregloCalles.darTamAct();i++) {
			Arco actual = (Arco) arregloCalles.darElementoI(i);
			String idOrigen = (String) actual.darOrigen().darId();
			String idDestino = (String) actual.darDestino().darId();
			grafo.adicionarArco(idOrigen, idDestino, actual);
			hashNodos.get(idOrigen).anadirArco(actual);
			hashNodos.get(idDestino).anadirArco(actual);
			System.out.println(idOrigen+"-"+idDestino+":"+actual.darCosto());
		}
	}
	@Override
	public void anadirEstacionesConSusArcos() {
		// TODO Auto-generated method stub
		for(int i=0;i<arregloStats.darTamAct();i++) {
			Station actual = (Station) arregloStats.darElementoI(i);
			double lat = actual.darLat();
			double lon = actual.darLon();
			String llave = darVerticeMasCercano(lat, lon);
			String key = "S-"+actual.getStationId();
			Vertice<String> agregar = new Vertice(actual,key);
			vertices.agregarElem(agregar);
			Vertice<String> otro = hashNodos.get(llave);
			System.out.println("El vertice: "+ agregar.darId()+" ha sido a�adido al grafo");
			double distancia = distancia(lat, lon, otro.darInfovertice().darLat(),otro.darInfovertice().darLon());
			grafo.adicionarVertice(key, agregar);
			Arco nuevo=new Arco(agregar, otro,distancia);
			grafo.adicionarArco(key, llave, nuevo);
			agregar.anadirArco(nuevo);
			otro.anadirArco(nuevo);
			arregloCalles.agregarElem(nuevo);
			System.out.println(agregar.darId()+"-"+llave+":"+nuevo.darCosto());
			arcosMixtos++;
		}

	}
	public String darVerticeMasCercano(double lat, double lon) {
		String ret = "";
		double menorDistancia = Double.MAX_VALUE;
		for(int i=0;i<vertices.darTamAct();i++) {
			Vertice<String> actual = (Vertice<String>) vertices.darElementoI(i);
			if(actual.darId().contains("I-")) {
				double lat2 = actual.darInfovertice().darLat();
				double lon2 = actual.darInfovertice().darLon();
				double distanciaActual= distancia(lat, lon, lat2, lon2);
				if(distanciaActual<=menorDistancia) {
					menorDistancia=distanciaActual;
					ret = actual.darId();
				}
			}
		}
		return ret;
	}
	@Override
	public void exportarJson() {
		// TODO Auto-generated method stub
		try {
			PrintWriter writer = new PrintWriter(new File("data/infoGrafo.csv"));
			writer.println(" ");
			for(int i=0;i<vertices.darTamAct();i++) {
				Vertice<String> actual = (Vertice<String>) vertices.darElementoI(i);
				String id = actual.darId();
				double latitud = actual.darInfovertice().darLat();
				double longitud = actual.darInfovertice().darLon();
				String cadena = id+","+latitud+","+longitud;
				writer.println(cadena);
			}
			writer.println("CAMBIO,CAMBIO,CAMBIO");
			for(int i=0;i<arregloCalles.darTamAct();i++) {
				Arco actual = (Arco) arregloCalles.darElementoI(i);
				String verOrigen = (String) actual.darOrigen().darId();
				String verDestino = (String) actual.darDestino().darId();
				double distancia = actual.darCosto();
				String cadena = verOrigen+","+verDestino+","+distancia;
				writer.println(cadena);
			}
			System.out.println("Se guardaron los datos del grafo en el archivo infoGrafo.csv");
			System.out.println("estaciones:"+verticesEstacion);
			System.out.println("arcosMixtos:"+arcosMixtos);
			System.out.println("verticesInterseccion"+verticesInterseccion);
			System.out.println("arcosIniciales"+arcosIniciales);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void importarJson() {
		verticesEstacion=0;
		arcosMixtos=0;
		verticesInterseccion=0;
		arcosIniciales=0;
		// TODO Auto-generated method stub
		hashNodos = new LinearProbingHash<String, Vertice<String>>(330000);
		vertices = new ArregloFlexible<Vertice<String>>(330000);
		arregloCalles = new  ArregloFlexible<Arco>(330000);
		arregloNodos = new ArregloFlexible<Interseccion>(330000);
		CSVReader lector;
		try {
			lector = new CSVReader(new FileReader("data/infoGrafo.csv"));
			String[] info = lector.readNext(); 
			boolean yaVertices=false;
			while(!yaVertices) {
				info = lector.readNext();
				if(info[0].equals("CAMBIO")) {
					yaVertices = true;
				}
				else {
					String id = info[0];
					double latitud = Double.parseDouble(info[1]);
					double longitud = Double.parseDouble(info[2]);
					int idd = Integer.parseInt(id.split("-")[1]);
					if(id.contains("I")) {
						Interseccion inter=new Interseccion(idd, latitud ,longitud);
						arregloNodos.agregarElem(inter);
						hashNodos.put(id, new Vertice<String>(inter,id));
						vertices.agregarElem(new Vertice<String>(inter,id));
						verticesInterseccion++;
						System.out.println("I-"+id+":"+latitud+"-"+longitud);
					}else {
						Station estacion = new Station(idd,latitud,longitud);
						System.out.println("Se carg� la estaci�n: "+id);
						hashNodos.put(id, new Vertice<String>(estacion,id));
						vertices.agregarElem(new Vertice<String>(estacion,id));
						//statQueue.enqueue(nodo);
						try {
							arregloStats.agregarElem(estacion);
						}catch(Exception e) {
							System.out.println("");
						}
						verticesEstacion++;
					}
				}
			}
			System.out.println(verticesEstacion);
			while((info = lector.readNext())!= null) {
				String id = info[0];
				Vertice<String> inicial = hashNodos.get(info[0]);
				Vertice<String> otro = hashNodos.get(info[1]);
				double distancia = Double.parseDouble(info[2]);
				Arco anadir = new Arco(inicial, otro,distancia);
				arregloCalles.agregarElem(anadir);
				arcosIniciales++;
				inicial.darId();
				otro.darId();
				System.out.println("V1:"+inicial.darId()+"V2:"+otro.darId()+"D:"+distancia);
			}

			lector.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Se han cargado los datos");
	}
	@Override
	public void hacerMapa() {
		// TODO Auto-generated method stub

	}



}
