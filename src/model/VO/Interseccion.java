package model.VO;

import model.dataStructures.ArregloFlexible;

public class Interseccion extends InfoVertice{
	private int id;
	
	public Interseccion(int pId, double pLat, double pLong){
		id=pId;
		lat=pLat;
		lon=pLong;
		adjacencias = new ArregloFlexible<Vertice>(401);
	}
	public int darId(){
		return id;
	}
	
	
}
