package view;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Scanner;

import controller.Controller;
import model.logic.manager;



public class View {

	public static void main(String[] args){

		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		Controller controlador = new Controller();
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();

			int option = linea.nextInt();
			switch(option)
			{
			
			case 1:  //Carga de datos 1C
				Controller.cargarInfo();
				break;
				
				
			case 2: //Req 1A
				
			
				Controller.cargarVerticesInterseccion();;
			
				break;
			
			case 3: //Req 2A
				Controller.cargarArcosInterseccion();
				break;
				
			case 4: //Req 3A
				Controller.cargarEstaciones();
				break;
				
			case 5: //Req 4A
				Controller.exportarJson();
				break;
				
			case 6: //Req 1B
				Controller.importarJson();
				break;
				
			case 7: //Req 2B
				Controller.hacerMapa();
				break;
				
			
			case 8: //Salir
				fin = true;
				linea.close();
				break;
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de Datos----------");
		System.out.println("-------------------- Taller 8   - 2018-2 ----------------------");
		System.out.println("1. Cargar los datos");
		System.out.println("2. Cargar los vertices interseccion al grafo");
		System.out.println("3. Cargar los arcos al grafo");
		System.out.println("4. Cargar los vertices estacion y sus arcos al grafo");
		System.out.println("5. Exportar Json con los datos actuales");
		System.out.println("6. Importar Json de un archivo");
		System.out.println("7. Cargar el mapa en Google Maps");
		System.out.println("8. Salir");

	}

	
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}

}
