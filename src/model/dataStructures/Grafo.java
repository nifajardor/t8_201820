package model.dataStructures;
import java.util.NoSuchElementException;

import model.dataStructures.SeparateChainingHash;
public class Grafo<K extends Comparable<K> ,V,A> {

	private int vertices;
	private int arcos;
	private SeparateChainingHash<K,ArregloFlexible<A>> adjacencias;
	private SeparateChainingHash<String,String> pesoArcos;
	private SeparateChainingHash<K,V> hashVertices;
	
	public Grafo(int v){
		if(v<0)throw new IllegalArgumentException("El numero de vertices no puede ser  negativo");
		vertices = v;
		arcos = 0;
		adjacencias = new SeparateChainingHash<K,ArregloFlexible<A>>(v);
		hashVertices = new SeparateChainingHash<K,V>(v);
	}
	public int darVertices() {
		return vertices;
	}
	public int darArcos() {
		return arcos;
	}
	public SeparateChainingHash<K,ArregloFlexible<A>> darAdjacencias(){
		return adjacencias;
	}
	public SeparateChainingHash<String,String> darPesoArcos(){
		return pesoArcos;
	}
	public SeparateChainingHash<K,V> darHashVertices(){
		return hashVertices;
	}
	public void adicionarVertice(K idVertice, V elVertice) {
		hashVertices.put(idVertice, elVertice);
	}
	public void adicionarArco(K idVerInicio,K idVerFinal,A infoArc) {
		ArregloFlexible<A> elArreglo1 = adjacencias.get(idVerInicio);
		ArregloFlexible<A> elArreglo2 = adjacencias.get(idVerFinal);
		if(elArreglo1 == null) {
			elArreglo1 = new ArregloFlexible<A>(2);
			adjacencias.put(idVerInicio, elArreglo1);;
		}
		if(elArreglo2 == null) {
			elArreglo2 = new ArregloFlexible<A>(2);
			adjacencias.put(idVerFinal, elArreglo2);;
		}
		elArreglo1.agregarElem(infoArc);
		elArreglo2.agregarElem(infoArc);
		//pesoArcos.put(idVerInicio+"->"+idVerFinal,infoArc.toString());
	}
	
}
