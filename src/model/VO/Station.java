package model.VO;

import java.time.LocalDate;
import java.time.LocalDateTime;

import model.dataStructures.ArregloFlexible;

public class Station extends InfoVertice implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private LocalDate startDate;	
	private int capacidad;
	//TODO Completar

	public Station(int stationId, String stationName, LocalDate startDate, double latit, double longitud,int capacidad) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.startDate = startDate;
		this.capacidad=capacidad;
		lat = latit;
		lon = longitud;
		adjacencias = new ArregloFlexible<Vertice>(401);
	}
	public Station(int idd, double llat, double llon) {
		stationId = idd;
		lat = llat;
		lon = llon;
	}

	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stu
		if(startDate.isAfter(o.getStartDate())){
			return 1;
		}else if(startDate.isBefore(o.getStartDate())){
			return -1;
		}else{
			return 0;
		}
		
	}
	public double getLat() {
		return lat;
	}
	public double getLong() {
		return lon;
	}
	public double getCp() {
		return capacidad;
	}
	public LocalDate getStartDate() {
		return startDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return stationId + ", " + stationName;
	}
}
