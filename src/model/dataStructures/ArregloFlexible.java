package model.dataStructures;

public class ArregloFlexible<T> {
	public static final int START = 0;
	public static final int STOP = 1;
	private int tamanoMax;
	private int tamanoAct;
	public Object elementos[];

	public ArregloFlexible(int max){
		elementos=(Object[])new Object[max];
		tamanoMax=max;
		tamanoAct=0;
	}
	public void agregarElem(Object dato){
		if(tamanoAct==tamanoMax){
			tamanoMax=2*tamanoMax;
			Object[] copia=elementos;
			elementos=(Object[])new Object[tamanoMax];
			for(int i=0;i<tamanoAct;i++){
				elementos[i]=copia[i];
			}
		}

		elementos[tamanoAct] = dato;

		tamanoAct++;
	}

	public Object darElementoI(int i) {
		return elementos[i];
	}
	public int darTamAct() {
		return tamanoAct;
	}

	public void anadirElementoPos(int pos, Object elem) {
		elementos[pos] = elem;
	}

	public int darTamano(){
		return tamanoAct;
	}
	public Object[] darElementos(){

		return elementos;
	}
	
	

	
}
