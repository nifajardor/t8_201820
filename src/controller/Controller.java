package controller;

import model.logic.*;
import java.time.LocalDateTime;

import API.IManager;


public class Controller {
    private static manager elManager = new manager();

   
    public static void cargarInfo(){
    	elManager.cargarDatos(manager.RUTA_NODOS, manager.RUTA_CALLES, manager.RUTA_ESTACIONES);
    }
    public static void cargarVerticesInterseccion() {
    	elManager.anadirVerticesInterseccion();
    }
    public static void cargarArcosInterseccion() {
    	elManager.anadirArcosAIntersecciones();
    }
    public static void cargarEstaciones() {
    	elManager.anadirEstacionesConSusArcos();
    }
    public static void exportarJson() {
    	elManager.exportarJson();
    }
    public static void importarJson() {
    	elManager.importarJson();
    }
    public static void hacerMapa() {
    	elManager.hacerMapa();
    }
}
