package model.dataStructures;

public class LinearProbingHash <K extends Comparable<K>,V>	 {
	private static final int INIT_CAPACITY = 4;
private int N;
private int M;
private K[] keys;
private V[] values;
public LinearProbingHash(){
	this(INIT_CAPACITY);
}
public LinearProbingHash(int capacity){
	M=capacity;
	N=0;
	keys=(K[]) new Comparable[M];
	values=(V[]) new Object[M];
}
public int size(){
	return N;
}
public boolean isEmpty(){
	if(N==0){
		return true;
	}else{
		return false;
	}
}
public boolean contains(K key){
	if(key!=null){
		if(get(key)!=null){
			return true;
		}
	}
	return false;
}
public int hash(K key){
	return (key.hashCode() & 0x7fffffff) % M;
}
public void resize(int capacity){
	LinearProbingHash<K,V> temporal=new LinearProbingHash<K,V>(capacity);
	for(int i=0;i<M;i++){
		if(keys[i]!=null){
			try{
			temporal.put(keys[i],values[i]);
			}catch(Exception e){
				
			}
		}
	}
	keys=temporal.keys;
	values=temporal.values;
	M=temporal.M;
}
public void put(K key, V value)throws Exception{
	if(key==null){
		throw new Exception("La llave es nula");
	}
	if(value==null){
		delete(key);
		return;
	}
	if(N>=3*M/4){
		resize(2*M);
	}
	int i;
	for(i=hash(key);keys[i]!=null;i=(i+1)%M){
		if(keys[i].equals(key)){
			values[i]=value;
			break;
		}
	}
	keys[i]=key;
	values[i]=value;
	N++;
}
public V get(K key){
	if(key!=null){
		for(int i=hash(key);keys[i]!=null;i=(i+1)%M){
			if(keys[i].equals(key)){
				return values[i];
			}
		}
	}
	return null;
}
public void delete(K key){
	if(key!=null){
		if(contains(key)==true){
			int i=hash(key);
			while(!key.equals(keys[i])){
				i=(i+1)%M;
			}
			keys[i]=null;
			values[i]=null;
			
			i=(i+1)%M;
			while(keys[i]!=null){
				K rehashKey=keys[i];
				V rehashValue=values[i];
				keys[i]=null;
				values[i]=null;
				N--;
				try{
				put(rehashKey,rehashValue);
				i=(i+1)%M;
				}catch(Exception e){
					
				}
			}
			N--;
			
			if(N>0&&N<=M/8){
				resize(M/2);
			}
			assert check();
		}
	}
}
public Iterable<K> keys(){
	Queue<K> queue=new Queue<K>();
	for(int i=0;i<M;i++){
		if(keys[i]!=null){
			queue.enqueue(keys[i]);
		}
	}
	return queue;
}
private boolean check(){
	if(N>=3*M/4){
		System.err.println("Tama�o M de la tabla: "+M+";Tama�o N del arreglo: "+N );
		return false;
	}
	for(int i=0;i<M;i++){
		if(keys[i]==null){
			continue;
		}else if(get(keys[i])!=values[i]){
			System.err.println("get["+keys[i]+"] = "+get(keys[i])+"; values[i] = "+values[i]);
			return false;
		}
	}
		return true;
}
}
